# Socket Demo

These files are part of an introduction to sockets. Two scripts are included.
One acts as a server; the other as a client. The server waits for socket
connections, receives an integer from the client, and sends that number of
paragraphs of text to the client. (The paragraphs are from Alice in Wonderland.)
Once the text is sent, the socket is closed. This is similar to how the HTTP
protocol works.

## Installation

1. Clone the repository

       cd /path/to/your/projects
       git clone https://gitlab.com/EMPUG/socket-demo.git

2. Make the repository directory the current directory:

       cd socket-demo

3. Install required packages in a virtual environment. On *nix or Mac OS, execute:

       python3 -m venv venv
       source venv/bin/activate
       pip install -r requirements.txt

### Activating the Python virtual environment

Each time you run the scripts, you must first activate the virtual environment:

    source venv/bin/activate

## Running the scripts

To run the server, execute the command:

    python server.py

By default, it will listen on port 9999. You can set the port with the `-p` or
`--port` option:

    python server.py -p 2024

The server will continue to run until you press Ctrl-c.

Next run the client with the command:

    python client.py HOST:PORT NUMBER_OF_PARAGRAPHS

For example:

    python client.py 192.168.1.151:9999 3

The client will receive the number of paragraphs requested and print them to the
console.

## Notes

`server.py` delivers text from [Alice in Wonderland](https://www.gutenberg.org/files/11/11-h/11-h.htm),
available from [Project Gutenberg](https://www.gutenberg.org).

These scripts use the [socket module](https://docs.python.org/3/library/socket.html),
which is part of the Python standard library.

The [Socket Programming HOWTO](https://docs.python.org/3/howto/sockets.html)
provides an excellent overview of socket programming in Python.
