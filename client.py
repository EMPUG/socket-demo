import socket

import click

BUFFER_SIZE = 1024


@click.command()
@click.argument('socket_address')
@click.argument('number_of_paragraphs', type=int)
def main(socket_address, number_of_paragraphs):
    # Get the host and port number.
    host = socket_address.split(':')[0]
    try:
        port = int(socket_address.split(':')[1])
    except (IndexError, ValueError):
        print('Error: invalid socket address')
        return

    # Open a socket and send the number of paragraphs we want.
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))
    s.sendall(str(number_of_paragraphs).encode('utf-8'))

    # Read the socket until it closes.
    data = b''
    while True:
        bytes_read = s.recv(BUFFER_SIZE)
        if not bytes_read:
            # The socket is closed.
            break
        data += bytes_read

    # Print the paragraphs.
    print(data.decode('utf-8'))


if __name__ == '__main__':
    main()
