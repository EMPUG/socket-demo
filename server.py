import json
import socket
import textwrap
import threading

import click

BUFFER_SIZE = 1024
DEFAULT_PORT = 9999
TEXT_FILE = 'alice.json'


@click.command()
@click.option('-p', '--port', default=DEFAULT_PORT, show_default=True)
def main(port):
    # Read paragraphs of text to send to client.
    text_data = json.loads(open(TEXT_FILE).read())
    paragraphs = text_data['paragraphs']

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((socket.gethostname(), port))
    server_socket.listen(5)
    print(f'Listening for connections on {socket.gethostbyname(socket.gethostname())}:{port}')

    try:
        while True:
            # Accept connections and spin off "client_sockets" and threads to handle them.
            (client_socket, address) = server_socket.accept()
            socket_thread = threading.Thread(target=send_paragraphs, args=(client_socket, address, paragraphs))
            socket_thread.run()
    except KeyboardInterrupt:
        print('\nExiting')


def send_paragraphs(client_socket, client_address, paragraphs):
    # Get the ID of this thread, to include in console messages.
    thread_id = threading.get_ident()

    # Read the socket to get the number of paragraphs to send.
    data = client_socket.recv(BUFFER_SIZE)
    try:
        number_of_paragraphs = int(data.decode('utf-8'))
    except ValueError:
        print(f'{thread_id}: {client_address}: Error: invalid number of paragraphs')
        client_socket.close()
        return

    # Create the text to send.
    text_to_send =  format_paragraphs(paragraphs, number_of_paragraphs)

    # Send the text and close the socket.
    print(f'{thread_id}: {client_address}: Sending {number_of_paragraphs} paragraphs')
    client_socket.sendall(text_to_send.encode('utf-8'))
    client_socket.close()
    print(f'{thread_id}: {client_address}: Closed the socket')


def format_paragraphs(paragraphs, number_of_paragraphs):
    # Nicely format a list of paragraphs and return text.
    paragraphs = paragraphs[0:number_of_paragraphs]
    lines = []
    for paragraph in paragraphs:
        lines.extend(textwrap.wrap(paragraph, width=80))
        lines.append('')
    text_to_send = '\n'.join(lines)
    return text_to_send


if __name__ == '__main__':
    main()
